# docker-prestissimo

[![](https://images.microbadger.com/badges/image/liquidfish/prestissimo.svg)](https://microbadger.com/images/liquidfish/prestissimo "Get your own image badge on microbadger.com")

[Composer](https://hub.docker.com/_/composer) Docker image with [Prestissimo](https://github.com/hirak/prestissimo) preinstalled for faster builds.

Refer to [Composer image](https://hub.docker.com/_/composer) for usage documentation.
